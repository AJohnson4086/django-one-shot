from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem

# Create your views here.


def todo_list(request):
    todo_list_list = TodoList.objects.all()
    context = {
        "todo_list_list": todo_list_list,
    }
    return render(request, "todos/todo_list.html", context)


def todo_items(request, id):
    todo_list_detail = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list_detail": todo_list_detail,
    }
    return render(request, "todos/todo_list_detail.html", context)